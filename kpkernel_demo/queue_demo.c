
#include "los_task.h"
#include "ohos_init.h"

#define MSG_QUEUE_LENGTH                                16
#define BUFFER_LEN                                      50

static unsigned int m_msg_queue;

/***************************************************************
* 函数名称: msg_write_thread
* 说    明: 队列写函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void data_write_thread(void *arg)
{
    unsigned int data = 0;
    unsigned int ret = LOS_OK;

    while (1)
    {
        data++;
        ret = LOS_QueueWrite(m_msg_queue, (void *)&data, sizeof(data), LOS_WAIT_FOREVER);
        if (LOS_OK != ret)
        {
            printf("%s write Message Queue data fail ret:0x%x\n", __func__, ret);
        }
        else
        {
            printf("%s write Message Queue data:%u %p\n", __func__, data , &data);
        }

        /*delay 1s*/
        LOS_Msleep(1000);
    }
}

#define SEND_STR "hello world"


void msg_write_thread(void *arg)
{
    char msg[24] = {0};
    unsigned int ret = LOS_OK;

    
    // memcpy(msg,SEND_STR,strlen(SEND_STR));

    int data = 0;

    while (1)
    {
        data++;
        sprintf_s(msg,24,"%s++%d",SEND_STR,data);
        ret = LOS_QueueWriteCopy(m_msg_queue, msg, strlen(msg), LOS_WAIT_FOREVER);
        if (LOS_OK != ret)
        {
            printf("%s write Message Queue msg fail ret:0x%x\n", __func__, ret);
        }
        else
        {
            printf("%s write Message Queue msg:%s\n", __func__, msg);
        }

        /*delay 1s*/
        LOS_Msleep(1000);
    }
}

/***************************************************************
* 函数名称: msg_read_thread
* 说    明: 队列读函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void data_read_thread(void *arg)
{
    unsigned int addr;
    unsigned int ret = LOS_OK;
    unsigned int *pData = NULL;
      /*delay 5s*/
    LOS_Msleep(5000);

    while (1)
    {
        /*wait for message*/
        ret = LOS_QueueRead(m_msg_queue, (void *)&addr, BUFFER_LEN, LOS_WAIT_FOREVER);
        if (ret == LOS_OK)
        {
            printf("addr = %p\n",addr);
            pData = addr;
            printf("%s read Message Queue data:%u\n", __func__, *pData);
        }
        else
        {
            printf("%s read Message Queue fail ret:0x%x\n", __func__, ret);
        }
    }
}


void msg_read_thread(void *arg)
{
    int ret;
    char msg[BUFFER_LEN]={0};

        /*delay 5s*/
    LOS_Msleep(5000);

    while (1)
    { 
        /*wait for message*/
     
        int read_len=BUFFER_LEN;

        ret = LOS_QueueReadCopy(m_msg_queue, msg, &read_len, LOS_WAIT_FOREVER);
        if (ret == LOS_OK)
        {
            // pData = addr;
            printf("%s read Message Queue msg len is %d,msg is:%s\n", __func__,read_len, msg);
        }
        else
        {
            printf("%s read Message Queue fail ret:0x%x\n", __func__, ret);
        }
    }
}
/***************************************************************
* 函数名称: queue_example
* 说    明: 内核队列函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void queue_example()
{
    printf(">> %s start\n",__func__);
    
    unsigned int thread_id1;
    unsigned int thread_id2;
    TSK_INIT_PARAM_S task1 = {0};
    TSK_INIT_PARAM_S task2 = {0};
    unsigned int ret = LOS_OK;

    ret = LOS_QueueCreate("queue", MSG_QUEUE_LENGTH, &m_msg_queue, 0, BUFFER_LEN);
    if (ret != LOS_OK)
    {
        printf("Falied to create Message Queue ret:0x%x\n", ret);
        return;
    }

    task1.pfnTaskEntry = (TSK_ENTRY_FUNC)msg_write_thread;
    task1.uwStackSize = 2048;
    task1.pcName = "msg_write_thread";
    task1.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id1, &task1);
    if (ret != LOS_OK)
    {
        printf("Falied to create msg_write_thread ret:0x%x\n", ret);
        return;
    }

    task2.pfnTaskEntry = (TSK_ENTRY_FUNC)msg_read_thread;
    task2.uwStackSize = 2048;
    task2.pcName = "msg_read_thread";
    task2.usTaskPrio = 25;
    ret = LOS_TaskCreate(&thread_id2, &task2);
    if (ret != LOS_OK)
    {
        printf("Falied to create msg_read_thread ret:0x%x\n", ret);
        return;
    }
    
}

// APP_FEATURE_INIT(queue_example);