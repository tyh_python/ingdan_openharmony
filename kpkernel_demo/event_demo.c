

#include "los_event.h"
#include "los_task.h"
#include "ohos_init.h"
//8421
#define EVENT_MASK1                                (0x00000001)
#define EVENT_MASK2                                (1<<2)
#define EVENT_MASK3                                (1<<3)

static EVENT_CB_S m_event;

/***************************************************************
* 函数名称: event_master_thread
* 说    明: 事件主线程函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void event_master_thread()
{
    unsigned int ret = LOS_OK;

    LOS_Msleep(1000);

    while (1)
    {
        printf("\n>>>>>>>>>>>>>>>\n");
        printf("%s write event:0x%x\n", __func__, EVENT_MASK1);
        ret = LOS_EventWrite(&m_event, EVENT_MASK1);
        if (ret != LOS_OK) {
            printf("%s write event failed ret:0x%x\n", __func__, ret);
        }

        /*delay 2s*/
        LOS_Msleep(2000);
    
        printf("\n=================\n");
        printf("%s write event:0x%x\n", __func__, EVENT_MASK2);
        ret = LOS_EventWrite(&m_event, EVENT_MASK2);
        if (ret != LOS_OK) {
            printf("%s write event failed ret:0x%x\n", __func__, ret);
        }

        /*delay 1s*/
        LOS_Msleep(1000);
        
    }
}

char name[24];

/***************************************************************
* 函数名称: event_slave_thread
* 说    明: 事件从线程函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void event_slave_thread()
{
    unsigned int event;

    while (1)
    {
        /* 阻塞方式读事件，等待事件到达*/
        event = LOS_EventRead(&m_event,
        EVENT_MASK1|EVENT_MASK2|EVENT_MASK3, 
        LOS_WAITMODE_OR, 
        LOS_WAIT_FOREVER);

        printf("%s read event:0x%x\n", __func__, event);
        if (event & EVENT_MASK1){
            //deal with event mask 1
            //buzzer on
        }

        if (event & EVENT_MASK2){
            //deal with event mask 2
            //led on
            LOS_EventClear(&m_event,~EVENT_MASK2);
        }
        
        // LOS_EventClear(&m_event,~event);
        // LOS_Msleep(1000);
    }
}

/***************************************************************
* 函数名称: event_example
* 说    明: 内核事件函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void event_example()
{
    printf("%s >> start\n");
    unsigned int thread_id1;
    unsigned int thread_id2;
    TSK_INIT_PARAM_S task1 = {0};
    TSK_INIT_PARAM_S task2 = {0};
    unsigned int ret = LOS_OK;

    ret = LOS_EventInit(&m_event);
    if (ret != LOS_OK)
    {
        printf("Falied to create EventFlags\n");
        return;
    }

    task1.pfnTaskEntry = (TSK_ENTRY_FUNC)event_master_thread;
    task1.uwStackSize = 2048;
    task1.pcName = "event_master_thread";
    task1.usTaskPrio = 25;
    ret = LOS_TaskCreate(&thread_id1, &task1);
    if (ret != LOS_OK)
    {
        printf("Falied to create event_master_thread ret:0x%x\n", ret);
        return;
    }

    task2.pfnTaskEntry = (TSK_ENTRY_FUNC)event_slave_thread;
    task2.uwStackSize = 2048;
    task2.pcName = "event_slave_thread";
    task2.usTaskPrio = 25;
    ret = LOS_TaskCreate(&thread_id2, &task2);
    if (ret != LOS_OK)
    {
        printf("Falied to create event_slave_thread ret:0x%x\n", ret);
        return;
    }
}

APP_FEATURE_INIT(event_example);