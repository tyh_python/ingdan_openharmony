
#include "los_task.h"
#include "ohos_init.h"

static unsigned int m_mutex_id;
static unsigned int m_data = 0;

void write_thread();
void read_thread();



/***************************************************************
 * 函数名称: mutex_example
 * 说    明: 内核互斥锁函数
 * 参    数: 无
 * 返 回 值: 无
 ***************************************************************/
void mutex_example()
{
    printf(">> mutex demo start!\n");
    unsigned int thread_id1;
    unsigned int thread_id2;
    TSK_INIT_PARAM_S task1 = {0};
    TSK_INIT_PARAM_S task2 = {0};
    unsigned int ret = LOS_OK;

    ret = LOS_MuxCreate(&m_mutex_id);
    if (ret != LOS_OK)
    {
        printf("Falied to create Mutex\n");
    }

    task1.pfnTaskEntry = (TSK_ENTRY_FUNC)write_thread;
    task1.uwStackSize = 2048;
    task1.pcName = "write_thread";
    task1.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id1, &task1);
    if (ret != LOS_OK)
    {
        printf("Falied to create write_thread ret:0x%x\n", ret);
        return;
    }

    task2.pfnTaskEntry = (TSK_ENTRY_FUNC)read_thread;
    task2.uwStackSize = 2048;
    task2.pcName = "read_thread";
    task2.usTaskPrio = 25;
    ret = LOS_TaskCreate(&thread_id2, &task2);
    if (ret != LOS_OK)
    {
        printf("Falied to create read_thread ret:0x%x\n", ret);
        return;
    }
}

// APP_FEATURE_INIT(mutex_example);


/***************************************************************
 * 函数名称: write_thread
 * 说    明: 写数据线程函数
 * 参    数: 无
 * 返 回 值: 无
 ***************************************************************/
void write_thread()
{
    while (1)
    {
        LOS_MuxPend(m_mutex_id, LOS_WAIT_FOREVER);

        m_data++;
        printf("!! current tick:%ld\n",LOS_TickCountGet());
        printf("write_thread write data:%u\n", m_data);

        LOS_Msleep(10000);
        LOS_MuxPost(m_mutex_id);
        printf("## current tick:%ld\n",LOS_TickCountGet());
    }
}

/***************************************************************
 * 函数名称: read_thread
 * 说    明: 读数据线程函数
 * 参    数: 无
 * 返 回 值: 无
 ***************************************************************/
void read_thread()
{
    /*delay 1s*/
    LOS_Msleep(1000);


    while (1)
    {
      
        printf(">> current tick:%ld\n",LOS_TickCountGet());
        LOS_MuxPend(m_mutex_id, LOS_WAIT_FOREVER);
        printf("read_thread read data:%u\n", m_data);

        LOS_Msleep(5000);
        LOS_MuxPost(m_mutex_id);
        printf("== current tick:%ld\n",LOS_TickCountGet());
    }
}
