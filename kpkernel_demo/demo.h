#ifndef __DEMO_H__
#define __DEMO_H__


void soft_timer_demo();
void task_example();
void mutex_example();
void semaphore_example();
void queue_example();
void event_example();

#endif