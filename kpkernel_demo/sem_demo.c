#include "los_sem.h"
#include "ohos_init.h"
#include "stdint.h"

#define MAX_COUNT                   2

static unsigned int m_sem;

#if 0
/***************************************************************
* 函数名称: control_thread
* 说    明: 控制线程函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void control_thread()
{
    unsigned int count = 0;

    while (1)
    {
        /*释放两次信号量，sem_one_thread和sem_two_thread同步执行;
        释放一次信号量，sem_one_thread和sem_two_thread交替执行*/
        if (count++%3)
        {
            LOS_SemPost(m_sem);
            printf("control_thread Release once Semaphore\n");
        }
        else
        {
            LOS_SemPost(m_sem);
            LOS_SemPost(m_sem);
            printf("control_thread Release twice Semaphore\n");
        }
        
        LOS_Msleep(1000);
    }
}

/***************************************************************
* 函数名称: sem_one_thread
* 说    明: 信号量线程函数1
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void sem_one_thread()
{
    while (1)
    {
        /*申请信号量*/
        LOS_SemPend(m_sem, LOS_WAIT_FOREVER);

        printf("sem_one_thread get Semaphore\n");
        LOS_Msleep(100);
    }
}

/***************************************************************
* 函数名称: sem_two_thread
* 说    明: 信号量线程函数2
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void sem_two_thread()
{
    while (1)
    {
        /*申请信号量*/
        LOS_SemPend(m_sem, LOS_WAIT_FOREVER);

        printf("sem_two_thread get Semaphore\n");
        LOS_Msleep(100);
    }
}
#endif

void task_getSem(uint32_t arg)
{
    int task_id = arg;
    printf("===== task %d start\n",task_id);
    while (1)
    {
        /*申请信号量*/
        printf("==> task %d ready to get Semaphore\n",task_id);
        LOS_SemPend(m_sem, LOS_WAIT_FOREVER);

        printf(">>> task %d have get Semaphore\n",task_id);
        LOS_Msleep(5000);
        LOS_SemPost(m_sem);
        printf("### task %d have release Semaphore\n",task_id);
        LOS_Msleep(3000);
    }

}

/***************************************************************
* 函数名称: semaphore_example
* 说    明: 内核信号量函数
* 参    数: 无
* 返 回 值: 无
***************************************************************/
void semaphore_example()
{
    printf(">> %s start\n",__func__);
    unsigned int thread_id1;
    unsigned int thread_id2;
    unsigned int thread_id3;
    unsigned int thread_id4;

    TSK_INIT_PARAM_S task1 = {0};
    TSK_INIT_PARAM_S task2 = {0};
    TSK_INIT_PARAM_S task3 = {0};
    TSK_INIT_PARAM_S task4 = {0};

    unsigned int ret = LOS_OK;

    ret = LOS_SemCreate(MAX_COUNT, &m_sem);
    if (ret != LOS_OK)
    {
        printf("Falied to create Semaphore\n");
        return;
    }

    task1.pfnTaskEntry = (TSK_ENTRY_FUNC)task_getSem;
    task1.uwStackSize = 2048;
    task1.uwArg=1;
    task1.pcName = "task1";
    task1.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id1, &task1);
    if (ret != LOS_OK)
    {
        printf("Falied to create task1 ret:0x%x\n", ret);
        return;
    }

    task2.pfnTaskEntry = (TSK_ENTRY_FUNC)task_getSem;
    task2.uwStackSize = 2048;
    task2.uwArg=2;
    task2.pcName = "task2";
    task2.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id2, &task2);
    if (ret != LOS_OK)
    {
        printf("Falied to create task2 ret:0x%x\n", ret);
        return;
    }

    task3.pfnTaskEntry = (TSK_ENTRY_FUNC)task_getSem;
    task3.uwStackSize = 2048;
    task3.uwArg=3;
    task3.pcName = "task3";
    task3.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id3, &task3);
    if (ret != LOS_OK)
    {
        printf("Falied to create task4 ret:0x%x\n", ret);
        return;
    }

    task4.pfnTaskEntry = (TSK_ENTRY_FUNC)task_getSem;
    task4.uwStackSize = 2048;
    task4.uwArg=4;
    task4.pcName = "task3";
    task4.usTaskPrio = 24;
    ret = LOS_TaskCreate(&thread_id4, &task4);
    if (ret != LOS_OK)
    {
        printf("Falied to create task3 ret:0x%x\n", ret);
        return;
    }  
    
}

APP_FEATURE_INIT(semaphore_example);